# 2024
# 2023
- Mathis Ersted Rasmussen et al.: [A simple single-cycle interactive strategy to improve deep learning-based segmentation of organs-at-risk in head-and-neck cancer](https://doi.org/10.1016/j.phro.2023.100426)
- Lasse Refsgaard et al.: [End-to-end framework for automated collection of large multicentre radiotherapy datasets demonstrated in a Danish Breast Cancer Group cohort](https://doi.org/10.1016/j.phro.2023.100485)
- Emma Skarsø Buhl et al.: [Development of a national deep learning-based auto-segmentation model for the heart on clinical delineations from the DBCG RT nation cohort](https://doi.org/10.1080/0284186x.2023.2252582)
- Laura Patricia Kaplan et al.: [Patient anatomy-specific trade-offs between sub-clinical disease coverage and normal tissue dose reduction in head-and-neck cancer](https://doi.org/10.1016/j.radonc.2023.109526)
# 2022
- Laura Patricia Kaplan et al.: [Plan quality assessment in clinical practice: Results of the 2020 ESTRO survey on plan complexity and robustness](https://doi.org/10.1016/j.radonc.2022.06.005)
- Jintao Ren et. al.: [Comparing Deep Learning and Conventional Machine Learning for Outcome Prediction of Head and Neck Cancer in PET/CT](https://doi.org/10.1007/978-3-030-98253-9_30)
- Jintao Ren et. al.: [PET Normalizations to Improve Deep Learning Auto-Segmentation of Head and Neck Tumors in 3D PET/CT](https://doi.org/10.1007/978-3-030-98253-9_7)

# 2021
- Jintao Ren et. al.: [Comparing different CT, PET and MRI multi-modality image combinations for deep learning-based head and neck tumor segmentation](https://doi.org/10.1080/0284186x.2021.1949034)
- Laura Patricia Kaplan et al.: [A systematically compiled set of quantitative metrics to describe spatial characteristics of radiotherapy dose distributions and aid in treatment planning](https://doi.org/10.1016/j.ejmp.2021.09.014)